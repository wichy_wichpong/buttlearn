package com.wichponggmail.k.butttt;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private Button asdasd;
    GetLocation gps;
    private TextView ertert;
    private int count;
    private double lat=-1;
    private double lng=-1;
    String allsegments;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                            Manifest.permission.ACCESS_FINE_LOCATION, false);
                    gps=new GetLocation(MainActivity.this);
                    if(gps.canGetLocation()){
                        lat=gps.getLat();
                        lng=gps.getLng();
                        Toast.makeText(getApplicationContext(),"Lat "+lat+"\n Long"+lng,Toast.LENGTH_SHORT).show();
                    }

                    }
//
            });


        asdasd = (Button) findViewById(R.id.getSeg);
        ertert = (TextView) findViewById(R.id.textView);

        asdasd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                new JSONTask().execute("https://www.strava.com/api/v3/segments/3878562");
                if(lat == -1 || lng==-1){
                    Toast.makeText(getApplicationContext(),"Please Get Location First",Toast.LENGTH_LONG).show();
                }else {
                    double topRightLat = lat + .008;
                    double topRightLng = lng + .008;
                    double bottomLeftLat = lat - .008;
                    double bottomLeftLng = lng - .008;
                    allsegments = null;
//                Toast.makeText(getApplicationContext(),"topright"+topRightLat+","+topRightLng+"\nbottomleft"+bottomLeftLat+","+bottomLeftLng,Toast.LENGTH_LONG).show();
                    new JSONTask().execute("https://www.strava.com/api/v3/segments/explore?bounds=" + bottomLeftLat + "," + bottomLeftLng + "," + topRightLat + "," + topRightLng);
                    new JSONTask().execute("https://www.strava.com/api/v3/segments/explore?bounds=13.653375,100.717749,13.717542,100.785547");
//                new JSONTask().execute("https://www.strava.com/api/v3/segments/explore?bounds=" + bottomLeftLat + "," + bottomLeftLng + "," + topRightLat + "," + topRightLng);
                }
            }
        });



//         ATTENTION: This was auto-generated to implement the App Indexing API.
//         See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.wichponggmail.k.butttt/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.wichponggmail.k.butttt/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
//
//        View.OnClickListener ourOnClickListener = new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                count++;
//                ertert.setText(Integer.toString(count));
//            }
//        };
//        asdasd.setOnClickListener(ourOnClickListener);
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            Toast toastmess = Toast.makeText(this,"asdasdad",Toast.LENGTH_LONG);
//            toastmess.show();
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
public class JSONTask extends AsyncTask<String,String,String> {

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected String doInBackground(String... params) {

        HttpURLConnection connection = null;
        BufferedReader reader = null;
        try {
            URL url = new URL(params[0]);

            connection = (HttpsURLConnection) url.openConnection();

            connection.setRequestProperty("Authorization", "Bearer " + "87c671c6966d8bfb297f4b5e0ca657411007392f");

            InputStream stream = connection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(stream));
            StringBuffer buffer = new StringBuffer();
            String line = "";
            while ((line=reader.readLine())!=null){
                buffer.append(line);
            }
            Log.d("RetrievedSegments", buffer.toString());
            System.out.println(buffer);
            return buffer.toString();
//            String retrieve =  buffer.toString();
//            JSONObject parentObject = new JSONObject(retrieve);;
//            JSONArray parent_array = parentObject.getJSONArray("segments");
//            JSONObject retrieveObject = parent_array.getJSONObject(0);
//            int id_seg = retrieveObject.getInt("id");
//            String name_seg = retrieveObject.getString("name");
//            return id_seg +" -- "+name_seg;
 
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
// catch (JSONException e) {
//            e.printStackTrace();
//        }
        finally {
            if(connection!=null)
                connection.disconnect();
            try {
                if(reader!=null)
                    reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;

    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(allsegments==null){
            allsegments=s;
        }
        else{
            allsegments=allsegments.substring(0,allsegments.length()-2)+","+s.substring(13,s.length());
        }
        ertert.setMovementMethod(new ScrollingMovementMethod());
        ertert.setText(allsegments);
    }
}
    public void showSegment(View view){
        if (allsegments == null){
            Toast.makeText(getApplicationContext(),"Get segments First",Toast.LENGTH_LONG).show();
        }
        else{
            Intent intent = new Intent(this,DisplaySegment.class);
            intent.putExtra("json_data", allsegments);
            startActivity(intent);
        }
    }




}
