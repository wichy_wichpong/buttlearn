package com.wichponggmail.k.butttt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wichpong on 14/1/2559.
 */
public class SegmentAdapter extends ArrayAdapter {
    List list = new ArrayList();
    public SegmentAdapter(Context context, int resource) {
        super(context, resource);
    }


    public void add(Segment object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        row =convertView;
        SegmentHolder segmentHolder;
        if(row==null){
            LayoutInflater layoutInflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.row_layout,parent,false);
            segmentHolder = new SegmentHolder();
            segmentHolder.tx_id = (TextView)row.findViewById(R.id.tx_id);
            segmentHolder.tx_name = (TextView)row.findViewById(R.id.tx_name);
            segmentHolder.tx_distance = (TextView)row.findViewById(R.id.tx_distance);
            row.setTag(segmentHolder);
        }else{
            segmentHolder = (SegmentHolder)row.getTag();
        }
        Segment segment = (Segment)this.getItem(position);
        segmentHolder.tx_id.setText(segment.getId());
        segmentHolder.tx_name.setText(segment.getName());
        segmentHolder.tx_distance.setText(segment.getDistance());

        return row;
    }
    static class SegmentHolder{
        TextView tx_id,tx_name,tx_distance;
    }
}
