package com.wichponggmail.k.butttt;

/**
 * Created by wichpong on 14/1/2559.
 */
public class Segment {
    private String id,name,distance;

    public Segment(String id,String name,String distance){
        this.setId(id);
        this.setName(name);
        this.setDistance(distance);
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }
}
