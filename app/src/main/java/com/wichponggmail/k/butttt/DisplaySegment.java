package com.wichponggmail.k.butttt;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DisplaySegment extends AppCompatActivity {
    String json_segment;
    JSONObject jsonObject;
    JSONArray jsonArray;
    SegmentAdapter segmentAdapter;
    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_segment);
        listView = (ListView)findViewById(R.id.listView);

        segmentAdapter = new SegmentAdapter(this,R.layout.row_layout);
        listView.setAdapter(segmentAdapter);
        json_segment = getIntent().getExtras().getString("json_data");
        try {
            jsonObject = new JSONObject(json_segment);
            jsonArray = jsonObject.getJSONArray("segments");
            int count=0;
            String name,id,distance;

            while(count<jsonArray.length()){
                JSONObject JO = jsonArray.getJSONObject(count);
                id=JO.getString("id").toString();
                name=JO.getString("name");
                distance=JO.getString("distance").toString();
                Segment segment = new Segment(id,name,distance);
                segmentAdapter.add(segment);
                count++;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
